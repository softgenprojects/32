import { Text, HStack, usePrefersReducedMotion, ScaleFade } from '@chakra-ui/react';
import { AiFillCode } from 'react-icons/ai';

export const BrandLogo = ({ size = '32px', color = 'gray.800', hoverColor = 'teal.500', ...props }) => {
  const prefersReducedMotion = usePrefersReducedMotion();

  const animation = prefersReducedMotion
    ? {}
    : {
        _hover: {
          transform: 'scale(1.2)',
          color: hoverColor,
        },
        transition: 'transform 0.2s ease-in-out, color 0.2s ease-in-out',
      };

  return (
    <HStack spacing={2} alignItems='center' {...animation} {...props}>
      <ScaleFade initialScale={0.9} in={true}>
        <AiFillCode size={size} />
      </ScaleFade>
      <Text fontSize='xl' fontWeight='bold' color={color}>
        Ivan Bagaric
      </Text>
    </HStack>
  );
};