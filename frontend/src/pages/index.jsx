import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, Image, useBreakpointValue, ScaleFade, Link, SimpleGrid, Icon, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { FaUserTie, FaBriefcase, FaPalette, FaGithub, FaLinkedin, FaTwitter } from 'react-icons/fa';
import { motion } from 'framer-motion';

const MotionBox = motion(Box);
const MotionIcon = motion(Icon);

const Home = () => {
  const buttonSize = useBreakpointValue({ base: 'sm', md: 'md' });
  const bgGradient = useColorModeValue(
    'linear(to-br, pink.500, purple.500)',
    'linear(to-br, pink.300, purple.300)'
  );
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <ScaleFade initialScale={0.9} in={true}>
      <MotionBox minH='100vh' p={{ base: 4, md: 8 }} bgGradient={bgGradient} animate={{ scale: [0.9, 1], opacity: [0.7, 1] }} transition={{ duration: 0.5 }}>
        <VStack spacing={8} textAlign='center' maxW='lg' mx='auto'>
          <BrandLogo boxSize={{ base: '100px', md: '150px' }} />
          <Heading as='h1' size='2xl' fontWeight='extrabold' color='whiteAlpha.900'>
            Ivan Bagaric, CTO & Co-Founder of SoftGen.ai
          </Heading>
          <Text fontSize='xl' color='whiteAlpha.800'>
            Mastering the art of code and design to build exceptional digital experiences.
          </Text>
          <SimpleGrid columns={{ base: 1, md: 3 }} spacing={5}>
            <Button as={Link} href='#about' leftIcon={<FaUserTie />} variant='solid' colorScheme='white' size={buttonSize} borderRadius='full' onClick={onOpen}>
              About Ivan
            </Button>
            <Button as={Link} href='#projects' rightIcon={<FaBriefcase />} variant='solid' colorScheme='white' size={buttonSize} borderRadius='full'>
              Projects
            </Button>
            <Button as={Link} href='#design' rightIcon={<FaPalette />} variant='solid' colorScheme='white' size={buttonSize} borderRadius='full'>
              Design Work
            </Button>
          </SimpleGrid>
          <Box>
            <MotionIcon as={FaGithub} boxSize={8} m={2} color='white' whileHover={{ scale: 1.2 }} />
            <MotionIcon as={FaLinkedin} boxSize={8} m={2} color='white' whileHover={{ scale: 1.2 }} />
            <MotionIcon as={FaTwitter} boxSize={8} m={2} color='white' whileHover={{ scale: 1.2 }} />
          </Box>
        </VStack>
      </MotionBox>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Ivan Bagaric's Bio</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>
              With over a decade of experience in software development, Ivan has a proven track record of leading teams to deliver cutting-edge solutions. His expertise spans across full-stack development, cloud computing, and AI technologies.
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme='blue' mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </ScaleFade>
  );
};

export default Home;
